﻿/* GMSCRMBOLEmailExtract V1.00 
 * Program is used to connect to TELUS CRM to extract Grocery Operation email list to provide GMS for BOL Email
 * 
 * To Run in Production, you will need to update the App.config file to change the endpoint for Telus Production site:
 * 
 * This program will extract the data from Telus CRM application and create a flat file for GMS
 * Validation is done on email address extracted and email sent to specified group configured in App.config
 * 
 *  RNUser - username to access Telus Oracle Cloud API/RightNow (provided by Telus)
 *  RNPass - password to access Telus Oracle Cloud API/RightNow (provided by Telus)
 *  PRODEndPoint - Production endpoint. Copy the url to address value in endpoint section in app.config if using Production
 *  
 *  <endpoint address="https://lcbo-en--rnt13.frontlinesvc.com/services/soap/connect/soap"
 *               binding="customBinding" bindingConfiguration="RightNowSyncBinding"
 *               contract="GRCRightNowReference.RightNowSyncPort" name="RightNowSyncPort" />
 * 
 * TESTEndPoint - Test/QA endpoint. see ProdEndPoint
 * DefaultEmail - This is the email address that will be included in the extract for each store.
 * FileDelimiter - This is delimiter that will be used for extraction. for Version 1.00 the | is being used.
 * EmailDelimiter - This is the separator that is being used to separate email addresses. for Version 1.00 the ; is being used.
 * LogFilePath - This is the path where Log files will be written to.
 * LogFileName - This is the naming convention of the Log file followed by date. for Version 1.00 GMSCRMBOLEmailExtract is being used.
 * EmailFilePath - This is the working directory for creating the file.
 * EmailFileName - This is the naming convention of the output file. for version 1.00 CRM_BOLdistribution_ is being used.
 * BadEmailRecipientList - this is the email recipient list to notify for bad email addresses.
 * GMSOutboundPath - This is the Path to outbound folder to drop file for GMS
 * ENV - this is the environment program is to run. It is used to compose the filepath for dropping off file to GMS 
 * 
 * Created By itszl, March 24, 2019
 */



using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Configuration;
using System.Data;
using System.Collections;
using System.IO;
using System.Globalization;
using GMSCRMBOLEmailExtract.GRCRightNowReference;
using EASendMail;


namespace GMSCRMBOLEmailExract
{
    class GMSCRMBOLEmail
    {
         static RightNowSyncPortClient _client;

        static GMSCRMBOLEmail()
        {
            _client = new RightNowSyncPortClient();
            _client.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["RNUSER"].ToString();
            _client.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["RNPass"].ToString();
        }      

        //Need LCBO Assigned Store #,Customer Number (Oracle),EDI number, Contact Email

         static DataTable getData(StreamWriter flog)
         {
             DataTable dtEmailList = new DataTable();

             string query = "SELECT LCBO_Assigned_Store, Store_Name, Customer_Number_Oracle, EDI_Number, BOL_Contact_1, BOL_Contact_2, BOL_Contact_3 FROM Grocery.StoreInfo WHERE Status=1 Order By LCBO_Assigned_Store ASC";
           //  string query = "SELECT * FROM Grocery.StoreInfo";            
             ClientInfoHeader clientInfoHeader = new ClientInfoHeader();
             clientInfoHeader.AppID = "Get Email BOL List";

             APIAccessRequestHeader apiRequestHeader = new APIAccessRequestHeader();
             APIAccessResponseHeaderType respHeaderType = new APIAccessResponseHeaderType();
           //  DataTable dt = new DataTable();
            
             byte[] rawResults = null;
             string delimiter = "~";


             try
             {
                CSVTableSet queryCSV = new CSVTableSet();
                respHeaderType = _client.QueryCSV(clientInfoHeader, apiRequestHeader, query, 10000, delimiter, false, true, out queryCSV, out rawResults);
              
                CSVTable[] csvTables = queryCSV.CSVTables;
                DataRow dr = null;
                foreach (CSVTable tbl in csvTables)
                {
                    string colHeadings = tbl.Columns;
                    //string tmpTable = "";
                    string[] rowdata = tbl.Rows;

                    string[] colnames = tbl.Columns.ToString().Split(delimiter.ToCharArray());


                    foreach (string col in colnames)
                    {
                        dtEmailList.Columns.Add(col);
                    }

                    //int counter = 0;
                    foreach (string data in rowdata)
                    {
                        int fldnum = 0;
                        string[] fld = data.Split(delimiter.ToCharArray());
                        dr = dtEmailList.NewRow();
                        foreach (string d in fld)
                        {
                            dr[fldnum] = d;
                            fldnum++;
                        }
                        dtEmailList.Rows.Add(dr);
                        
                    }
                }
             }
             catch (Exception ex)
             {
                 WriteLog(flog,"Error occurred...." + ex.Message.ToString());
             }


             return dtEmailList;
         }
         static void WriteLog(StreamWriter fw, string msg)
         {
             try
             {

                 fw.WriteLine(DateTime.Now.ToString("yyyyMMddHHmmss") + ": " + msg);
             }
             catch (Exception ex)
             {
                 Console.WriteLine("Error writing to Log file!" + ex.Message.ToString());
             }
                         
         }
         static void TestValidEmail(string emailaddress)
         {
             string testemail = emailaddress;
             if (isValidEmail(testemail))
             {
                 Console.WriteLine(testemail + " is valid");
             }
             else
             {
                 Console.WriteLine("ERROR: " + testemail + " is not valid!");
             }
             Console.ReadLine();
         
         }
         static bool isValidEmail(string emailaddress)
         {

             /* How to Test email  
              * 
              * call TestValidEmail(emailaddress) in main
              * 
             */
             bool valid = false;
           //  SmtpMail oMail = new SmtpMail("TryIt");
           //  SmtpClient oSmtp = new SmtpClient();
           //  oMail.From = "testemail@testemail.com";
           //  oMail.To = emailaddress;
           //  SmtpServer oServer = new SmtpServer("");

             Regex re = new Regex(@"(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|""(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*"")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])");

             valid = re.IsMatch(emailaddress.ToLower());

             /* Not all DNS allows for testing...taking this off the validation
             try
             {
                //Test Regex first before even attempting to use smtp to validate
                                              
                 if (re.IsMatch(emailaddress))
                 {
                    oSmtp.TestRecipients(oServer, oMail);
                    valid = true;
                 }
                 else
                 {
                     valid = false;
                 }
             }
             catch (SmtpTerminatedException exp)
             {
                 Console.WriteLine(exp.Message);
             }
             catch (SmtpServerException exp)
             {
                 Console.WriteLine("Exception: Server Respond: {0}", exp.ErrorMessage);
             }
             catch (System.Net.Sockets.SocketException exp)
             {
                 //by-passing network
                 Console.WriteLine("Exception: Networking Error: {0} {1}", exp.ErrorCode, exp.Message);
                 valid = true; 
             }
             catch (System.ComponentModel.Win32Exception exp)
             {
                 Console.WriteLine("Exception: System Error: {0} {1}", exp.ErrorCode, exp.Message);
             }
             catch (System.Exception exp)
             {
                 Console.WriteLine("Exception: Common: {0}", exp.Message);
             }
             */

             return valid;
         }

         static void SendInvalidEmailAddress(ArrayList InvalidEmailAddressList, StreamWriter Flog)
         {
             string strEmailRecipientList = ConfigurationManager.AppSettings["BadEmailRecipientList"].ToString();
             SmtpMail oMail = new SmtpMail("TryIt");
             SmtpClient oSmtp = new SmtpClient();
             oMail.From = "GMSBOLEmailExtractProgram@lcbo.com";
             oMail.To = strEmailRecipientList;
             oMail.Subject = "DO NOTE REPLY: GMS BOL Extract - Invalid Email found! " + DateTime.Now.ToString("yyyy-MM-dd") ;
             SmtpServer oServer = new SmtpServer("mail.lcbo.com");

             StringBuilder strEmailText = new StringBuilder();
             try
             {
                   strEmailText.Append("Please correct the below email addresses found in for BOL Email Extract\n\n");

                   foreach (string emailaddress in InvalidEmailAddressList)
                   {
                       strEmailText.Append(emailaddress + "\n");
                   }

                   strEmailText.Append("\n\n");
                   strEmailText.Append("GMS BOL Email address Extraction Program");


                   oMail.TextBody = strEmailText.ToString();

                   oSmtp.SendMail(oServer, oMail);


             }
             catch (Exception ex)
             {
                 WriteLog(Flog, "ERROR: Invalid Email List failed to be sent. " + ex.Message.ToString());
             }

         }
        static void Main(string[] args)
        {                                                                                     

            //TestValidEmail("..terry.xavie@gmail.com");
            //Console.ReadLine();
            //return;

          
            string LogFileName = ConfigurationManager.AppSettings["LogFilePath"].ToString() + "\\" + ConfigurationManager.AppSettings["LogFileName"].ToString() + DateTime.Now.ToString("yyyyMMdd") + ".log";
            FileStream fileLog = File.Open(LogFileName, FileMode.Append);
            StreamWriter flog = new StreamWriter(fileLog);
            DataTable dtEmailList = null;
            string defaultEmail = ConfigurationManager.AppSettings["DefaultEmail"].ToString();
            string fdelimiter = ConfigurationManager.AppSettings["FileDelimiter"].ToString();
            string edelimiter = ConfigurationManager.AppSettings["EmailDelimiter"].ToString();
            ArrayList lstBadEmail = new ArrayList();

            try
            {
              
                WriteLog(flog, "Program GMSCRMBOLEmail V1.00 started");
                WriteLog(flog, "Getting Data from CRM...");
                dtEmailList = getData(flog);
               
                WriteLog(flog, "Data retrieved from CRM. Preparing Data for writing...");

                ArrayList lstFileContent = new ArrayList();

                //Parse Data to write to File
                foreach (DataRow drEmail in dtEmailList.Rows)
                {
                    ArrayList lstEmail = new ArrayList();
                    //strEmail.Add(defaultEmail);

                    StringBuilder strList = new StringBuilder();
                    strList.Append(drEmail["LCBO_Assigned_Store"].ToString() + fdelimiter);
                    strList.Append(drEmail["Customer_Number_Oracle"].ToString() + fdelimiter);

                    //Need to check if it is Scientific notation
                    Int32 edinum = 0;
                    string strEDINum = "";
                    if (Int32.TryParse(drEmail["EDI_Number"].ToString(), out edinum))
                    {
                        strEDINum = Int32.Parse(drEmail["EDI_Number"].ToString()).ToString();
                    }
                    
                   
                    //strList.Append(drEmail["EDI_Number"].ToString() + fdelimiter);
                    strList.Append(strEDINum + fdelimiter);

                    
                    if (string.IsNullOrEmpty(drEmail["BOL_Contact_1"].ToString()) == false)
                    {

                        if (isValidEmail(drEmail["BOL_Contact_1"].ToString()) == true)
                        {
                            lstEmail.Add(drEmail["BOL_Contact_1"].ToString());
                        }
                        else
                        {
                            lstBadEmail.Add(drEmail["LCBO_Assigned_Store"].ToString() + "|" + drEmail["Store_Name"].ToString() + "|" + drEmail["BOL_Contact_1"].ToString());
                        }
                    }
                    
                    if (string.IsNullOrEmpty(drEmail["BOL_Contact_2"].ToString()) == false)
                    {
                        if (isValidEmail(drEmail["BOL_Contact_2"].ToString()) == true)
                        {
                            lstEmail.Add(drEmail["BOL_Contact_2"].ToString());
                        }
                        else
                        {
                            lstBadEmail.Add(drEmail["LCBO_Assigned_Store"].ToString() + "|" + drEmail["Store_Name"].ToString() + "|" + drEmail["BOL_Contact_2"].ToString());
                        }
                    }
                    if (string.IsNullOrEmpty(drEmail["BOL_Contact_3"].ToString()) == false)
                    {
                        if (isValidEmail(drEmail["BOL_Contact_3"].ToString()) == true)
                        {     
                            lstEmail.Add(drEmail["BOL_Contact_3"].ToString());
                        }
                        else
                        { 
                            lstBadEmail.Add(drEmail["LCBO_Assigned_Store"].ToString() + "|" + drEmail["Store_Name"].ToString() + "|" + drEmail["BOL_Contact_3"].ToString());
                        }
                    }

                    string strEmailBOLList = defaultEmail;
                    foreach (string emailaddress in lstEmail)
                    {
                        strEmailBOLList = strEmailBOLList + edelimiter + emailaddress.ToLower();
                    }

                    //Add to the line
                    strList.Append(strEmailBOLList);

                    lstFileContent.Add(strList.ToString());   

                }

                WriteLog(flog, "Completed data processing...There were " + lstFileContent.Count + " records found");
                String outputFilePath = ConfigurationManager.AppSettings["EmailFilePath"].ToString();
                String outputFilename = ConfigurationManager.AppSettings["EmailFileName"].ToString() + 
                    DateTime.Now.ToString("yyyyMMddHHmmss") + ".txt";

                if (lstFileContent.Count > 0)
                {
                    WriteLog(flog, "Preparing to write records to " + outputFilename);
                    //OK Write to file now

                    FileStream fileout = File.Open(outputFilePath + "\\" + outputFilename, FileMode.OpenOrCreate);
                    StreamWriter fout = new StreamWriter(fileout);

                    WriteLog(flog, "File Stream set successfully...writing records now...");

                    foreach (string fileline in lstFileContent)
                    {
                        fout.WriteLine(fileline);
                    }
                    fout.Close();

                    string outboundfolder = ConfigurationManager.AppSettings["GMSOutboundPath"].ToString() + "\\" + ConfigurationManager.AppSettings["ENV"].ToString() + "\\Outbound";

                    WriteLog(flog, "Sending file to Outbound folder: " + outputFilePath + "\\" + outputFilename + ", " +  outboundfolder + "\\" + outputFilename);

                    File.Copy(outputFilePath + "\\" + outputFilename, outboundfolder + "\\" + outputFilename, true);

                    WriteLog(flog, "File records written successfully.");

                    if (lstBadEmail.Count > 0)
                    {
                        WriteLog(flog, "ERROR: There were " + lstBadEmail.Count + " invalid email addresse(s) found.");
                        WriteLog(flog,"Sending email list to : " + ConfigurationManager.AppSettings["BadEmailRecipientList"].ToString());

                        SendInvalidEmailAddress(lstBadEmail, flog);

                    }
                    
                }
                else
                {
                    WriteLog(flog, "Sumting wong...0 records found.");
                }

                WriteLog(flog, "Program ended");
                WriteLog(flog, "=======================================================================================================================");

            }
            catch (Exception ex)
            {
                WriteLog(flog, "sumting wong..." + ex.Message.ToString());
            }
            finally
            {
                flog.Close();
            }

        }
    }
}
